
# coding: utf-8

# In[ ]:

#import required packages
import openpyxl
import xlwt
import os
import datetime
#Make sure jaydebeapi is version 0.2.0
import jaydebeapi as jd
import pandas as pd
from connect import UID,PWD
from connections import conn_string, jdbc_nzo, driver, jar_path
from email_credentials import userid,password

#This line establishes the connection to the server
conn = jd.connect(driver, jdbc_nzo, jar_path)
cursor = conn.cursor()         #open a cursor   


# In[ ]:

#Defining the query using standard SQL. The below query pulls data from legacy database
query_4="""select lgl_entity_nbr
		,lgl_entity_nm
		,Vault_Legacy
        ,cal_dt
		,count(*) as triggers
		,sum(printed_flg) as prints
from  
(select lgl_entity_nbr
		,lgl_entity_nm
		,'Legacy' as Vault_Legacy
		,ord_event_key
		,promo_src_id_txt
		,case when event_typ_cd ='IS-PRINT-T' then 1 else 0 end as printed_flg
        ,cal_dt
from ord_promo_varnt_fact_v o
join promotion_variant_v x on (o.promo_varnt_key = x.promo_varnt_key)
join touchpoint_v t on(ord_touchpoint_key=touchpoint_key)
join date_v d on (ord_date_key=date_key)
where event_typ_cd not in ('IS-PRINT-q','IS-PRINT-h','IS-PRINT-j','IS-PRINT-k','IS-PRINT-5','IS-PRINT-4')
and cal_dt between (CURRENT_DATE - extract(dow from CURRENT_DATE))-(7*4)+1 and (CURRENT_DATE - extract(dow from CURRENT_DATE))
and x.pgm_data_src_cd!='US-vlt'
and x.promo_funding_typ_cd = 'RTLR PAID') as t
group by 1,2,3,4;"""

legacy_ad_level=pd.read_sql_query(query_4, conn)


# In[ ]:

#Below query pulls the data from Vault
query_5="""select lgl_entity_nbr
             ,lgl_entity_nm
             ,cal_dt 
             ,'Vault' as Vault_Legacy   
             ,sum(printed) as prints      
             ,sum(case when printed = 1 or (id_control=0 and str_control=0 and sbe=0 and bloyal=0 and ployal=0 and SNSTV_CAT=0) then 1 else 0 end) as triggers 
from (
select promo_src_id_txt
		,ord_event_key
		,cal_dt
		,pgm_nm
		,targeting_typ_cd
		,promo_desc_txt
		,client_nm
		,promo_funding_typ_cd
     	,lgl_entity_nbr
     	,lgl_entity_nm
     	,ntwk_id
     	,ntwk_nm
     	,data_src_cd
        ,max(printed_flg) as printed
 		,max(case when event_typ_cd = 'IS-PRINT-h' then 1 else 0 end) as is_print_h
 		,max(case when event_typ_cd = 'BRAND_LOYL' then 1 else 0 end) as BRAND_LOYL
           /* OFFLINE AND VOIDS */
           ,max(offline_flg) as offline
           ,max(ie_flg) as ie
           ,max(yvoid_flg) as yvoid
           ,max(ievoid_flg) as ievoid
 
           /* CONTROLS */
           ,max(id_control_flg) as id_control
           ,max(str_control_flg) as str_control
 
           /* BUSINESS RULES */
           ,max(sbe_flg) as sbe
           ,max(bloyal_flg) as bloyal
           ,max(ployal_flg) as ployal
           ,max(SNSTV_CAT_flg) as SNSTV_CAT
 
           ,max(ao_flg) as ao
 
     from ord_promo_varnt_fact_v o 
     join (select distinct pv.promo_varnt_key
     				,promo_src_id_txt
     				,pgm_nm
     				,targeting_typ_cd
     				,promo_desc_txt
     				,client_nm
     				,promo_funding_typ_cd
			from promotion_variant_v pv 
			where pgm_data_src_cd='US-vlt') p using(promo_varnt_key)
     join touchpoint_v t on(ord_touchpoint_key=touchpoint_key)
     join date_v d on(ord_date_key=date_key)
     join event_flags_cr using(event_typ_cd)
     where data_src_cd != 'vlt-MFD'
     and promo_funding_typ_cd = 'RTLR PAID'
     and cal_dt between (CURRENT_DATE - extract(dow from CURRENT_DATE))-(7*4)+1 and (CURRENT_DATE - extract(dow from CURRENT_DATE))
     group by 1,2,3,4,5,6,7,8,9,10,11,12,13
union
select 'MFD' as promo_src_id_txt
		,ord_event_key
		,cal_dt
     	,'MFD' as pgm_nm
     	,'CB' as targeting_typ_cd
     	,'MFD' as promo_desc_txt
     	,'MFD' as client_nm
     	,promo_funding_typ_cd
     	,lgl_entity_nbr
     	,lgl_entity_nm
     	,ntwk_id
     	,ntwk_nm
     	,data_src_cd
        ,max(printed_flg) as printed
 		,max(case when event_typ_cd = 'IS-PRINT-h' then 1 else 0 end) as is_print_h
 		,max(case when event_typ_cd = 'BRAND_LOYL' then 1 else 0 end) as BRAND_LOYL 
           /* OFFLINE AND VOIDS */
           ,max(offline_flg) as offline
           ,max(ie_flg) as ie
           ,max(yvoid_flg) as yvoid
           ,max(ievoid_flg) as ievoid
 
           /* CONTROLS */
           ,max(id_control_flg) as id_control
           ,max(str_control_flg) as str_control
 
           /* BUSINESS RULES */
           ,max(sbe_flg) as sbe
           ,max(bloyal_flg) as bloyal
           ,max(ployal_flg) as ployal
           ,max(SNSTV_CAT_flg) as SNSTV_CAT
 
           ,max(ao_flg) as ao
 
     from ord_promo_varnt_fact_v o 
     join (select distinct pv.promo_varnt_key
     				,promo_src_id_txt
     				,pgm_nm
     				,targeting_typ_cd
     				,promo_desc_txt
     				,client_nm
     				,promo_funding_typ_cd
			from promotion_variant_v pv 
			where pgm_data_src_cd='US-vlt') p using(promo_varnt_key)
                join touchpoint_v t on(ord_touchpoint_key=touchpoint_key)
                join date_v d on(ord_date_key=date_key)
                join event_flags_cr using(event_typ_cd)
     where data_src_cd='vlt-MFD'
     and promo_funding_typ_cd = 'RTLR PAID'
     and cal_dt between (CURRENT_DATE - extract(dow from CURRENT_DATE))-(7*4)+1 and (CURRENT_DATE - extract(dow from CURRENT_DATE))
     group by 1,2,3,4,5,6,7,8,9,10,11,12,13 ) rho
    group by 1,2,3,4;"""

basket_ad_level2=pd.read_sql_query(query_5, conn)


# In[ ]:

#Close the connection
cursor.close() #close the cursor
conn.close()


# In[ ]:

#Converting the triggers column to float datatype
legacy_ad_level["triggers"]=legacy_ad_level.triggers.astype(str)
legacy_ad_level["triggers"]=legacy_ad_level.triggers.astype(float)


# In[ ]:

#combine legacy_ad_level and basket_ad_level2 dataframes and sort by legal entity number, date
df=pd.concat([basket_ad_level2,legacy_ad_level])
df.sort_values(by=['lgl_entity_nbr','cal_dt'],inplace=True)


# In[ ]:

#reindexing the columns
data=df.reindex(columns=['lgl_entity_nbr','lgl_entity_nm','cal_dt','vault_legacy','prints','triggers'])


# In[ ]:

#get the path of the reference report
oldreportpath=os.path.abspath("Reference Report.xlsx")  #----please provide the path of your reference report----


# In[ ]:

#load reference report data
oldreport = openpyxl.load_workbook(oldreportpath)
oldsheet=oldreport.get_sheet_by_name('Data')


# In[ ]:

#just to see a value in the cell
oldsheet.cell(13,5).value


# In[ ]:

#calculate number of columns in the new data
newcolumns=len(data.columns)
newcolumns


# In[ ]:

#calculate number of rows in the new data
newrows=len(data)
newrows


# In[ ]:

#Number of rows in the old report. This helps to get rid of extra records.
i=1
oldrows=0
for item in range(0,10000):
    if oldsheet.cell(i,1).value == None:
        oldrows=i-1
        break
    else:
        i=i+1
oldrows


# In[ ]:

#paste data to old report
def pasteRange(startCol, startRow, endCol, endRow, sheetReceiving,copiedData):
    countRow = 0
    for i in range(startRow,endRow+1,1):
        countCol = 0
        for j in range(startCol,endCol+1,1):           
            sheetReceiving.cell(row = i, column = j).value = copiedData.iloc[countRow][countCol]
            countCol += 1
        countRow += 1


# In[ ]:

pasteRange(1, 2, len(data.columns),len(data),oldsheet,data)


# In[ ]:

#to get rid of old data
def blankPasteRange(startCol, startRow, endCol, endRow, sheetReceiving):
    countRow = 0
    for i in range(startRow,endRow+1,1):
        countCol = 0
        for j in range(startCol,endCol+1,1):
            
            sheetReceiving.cell(row = i, column = j).value = None
            countCol += 1
        countRow += 1


# In[ ]:

if oldrows>newrows:
   blankPasteRange(1, newrows+1, len(data.columns),oldrows,oldsheet)


# In[ ]:

#copy the dates
def copyDate(startRow, endRow, sheet):
    rangeSelected = []
    #Loops through selected Rows
    for i in range(startRow,endRow,1):
        #Appends the row to a RowSelected list
        rowSelected = []
        rowSelected.append(sheet.iloc[i,2])
        #Adds the RowSelected List and nests inside the rangeSelected
        rangeSelected.append(rowSelected)
 
    return rangeSelected


# In[ ]:

dates=copyDate(0,newrows,data)


# In[ ]:

from datetime import datetime


# In[ ]:

#select the unique dates and sort them.
x=[]
for i in range(1,int(len(dates)/2)):
    a=dates[i]
    b=a.pop()
    c=datetime.strptime(b, '%Y-%m-%d').date()
    x.append(c)
x=set(x)
x=list(x)
x.sort()


# In[ ]:

#load other sheets to make changes to date
oldsheetLegacy=oldreport.get_sheet_by_name('Legacy')
oldsheetVault=oldreport.get_sheet_by_name('Vault')
oldsheetLegacyVault=oldreport.get_sheet_by_name('Legacy&Vault')
oldsheetCharts=oldreport.get_sheet_by_name('Charts')


# In[ ]:

#update dates
def pasteRange(startCol,startRow, endCol, sheetReceiving,copiedData):
        countCol = 0
        i=0
        for j in range(startCol,endCol+2,1):       
            sheetReceiving.cell(row = startRow, column = j).value = copiedData[countCol]
            countCol += 1
            i+=1


# In[ ]:

#update the dates in the relevant sheets
pasteRange(2,3,len(x),oldsheetLegacy,x)
pasteRange(2,3,len(x),oldsheetVault,x)
pasteRange(2,3,len(x),oldsheetLegacyVault,x)
pasteRange(2,1,len(x),oldsheetCharts,x)


# In[ ]:

#save the new report to your desired location.
oldreport.save('Print Trigger Report Retail Funded ' + str(max(x))+ '.xlsx')


# In[ ]:

newreport='Print Trigger Report Retail Funded ' + str(max(x))+ '.xlsx'


# In[ ]:

newreport


# In[ ]:

import smtplib as sm
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders


# In[ ]:

email_user='suhas.nadiga@catalina.com'    #----please enter your Catalina mail ID----
email_to='michael.ebert@catalina.com'
email_cc=['alice.fang@catalina.com','yimei.rogier@catalina.com','suhas.nadiga@catalina.com','annabelle.chen@catalina.com','bonnie.garrison@catalina.com','eric.bird@catalina.com']
subject='Contact Rate Report - '+str(max(x))


# In[ ]:

rcpt=[email_to]+email_cc
msg=MIMEMultipart()
msg['From']=email_user
msg['To']=email_to
msg['Cc']=",".join(email_cc)
msg['Subject']=subject


# In[ ]:

body="Please find the attached print trigger report for this week"
msg.attach(MIMEText(body,'plain'))

filename=newreport
attachment=open(filename,'rb')

part=MIMEBase('application','octet-stream')
part.set_payload((attachment).read())
encoders.encode_base64(part)
part.add_header('Content-Disposition',"attachment; filename= "+filename)


# In[ ]:

msg.attach(part)
text=msg.as_string()
server=sm.SMTP('smtp.office365.com',587)
server.starttls()


# In[ ]:

server.login(userid,password) #----Please provide your Catalina ID (Ex: snadiga@catmktg.com) and password


# In[ ]:

server.sendmail(email_user,rcpt,text)
server.quit()


# In[ ]:



