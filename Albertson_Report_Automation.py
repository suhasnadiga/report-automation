
# coding: utf-8

# In[1]:

#Make sure jaydebeapi is version 0.2.0
import jaydebeapi as jd
import pandas as pd
#import required packages
import openpyxl
import xlwt
import os
import datetime
from connect import UID,PWD
from connections import conn_string, jdbc_nzo, driver, jar_path
from email_credentials import userid,password

#This line establishes the connection to the server
conn = jd.connect(driver, jdbc_nzo, jar_path)
cursor = conn.cursor()         #open a cursor  


# In[ ]:

#Define your query here using standard SQL
query_1 ="""select d.touchpoint_hier_l2_desc
		,d.touchpoint_hier_l2_cd
		,b.cal_sun_wk_ending_dt
		,sum(a.tot_ord_amt) as dollars
		,count(distinct a.ord_event_key) as trips
                    from ord_sum_fact_v a
                          join date_v b on a.ord_date_key=b.date_key
                          join 
                          (select distinct touchpoint_hier_l2_desc, touchpoint_hier_l2_cd, tp.touchpoint_key
                          from touchpoint_v tp 
                          join touchpoint_hier_c_lgcy_v d on d.touchpoint_hier_l2_cd=tp.csdb_chn_nbr ) d on a.ord_touchpoint_key=d.touchpoint_key
                          join consumer_id_v c on a.ord_designated_cnsmr_id_key=c.cnsmr_id_key
                where     b.cal_dt between '3/7/16' and now()-2
                          and d.touchpoint_hier_l2_cd in (4,14,15,17,18,20,88,95,217,436,437,8,100,104,240,275,277,278,284,433,434,435,441)
                          and a.tot_ord_amt>=.15
                          group by 1,2,3;"""


total = pd.read_sql_query(query_1, conn)

query_2="""SELECT  
                     d.touchpoint_hier_l2_desc,d.touchpoint_hier_l2_cd,b.cal_sun_wk_ending_dt,sum(a.tot_ord_amt) as dollars,
                                 count(distinct a.ord_event_key) as trips, count(distinct c.cnsmr_id_nbr) as loyalty_ids
                     from ord_sum_fact_v a
                           join date_v b on a.ord_date_key=b.date_key
                           join (select distinct touchpoint_hier_l2_desc, touchpoint_hier_l2_cd, tp.touchpoint_key
                          from touchpoint_v tp 
                          join touchpoint_hier_c_lgcy_v d on d.touchpoint_hier_l2_cd=tp.csdb_chn_nbr ) d on a.ord_touchpoint_key=d.touchpoint_key
                           join consumer_id_v c on a.ord_designated_cnsmr_id_key=c.cnsmr_id_key                          
                     where b.cal_dt between '3/7/16' and now()-2
                           and d.touchpoint_hier_l2_cd in (4,14,15,17,18,20,88,95,217,436,437,8,100,104,240,275,277,278,284,433,434,435,441)
                           and a.tot_ord_amt>=.15
                           and a.cnsmr_id_typ_loy_cd = 'LOYL' 
                           and c.cnsmr_id_txt not in ('00000000046002000000')
                           group by 1,2,3;"""

loyalty = pd.read_sql_query(query_2, conn)


# In[ ]:

#Close the connection
cursor.close() #close the cursor
conn.close()


# In[ ]:

#join the required tables
alb_cos_loy=pd.merge(total,loyalty[['dollars','trips','loyalty_ids','touchpoint_hier_l2_cd','cal_sun_wk_ending_dt']],how='left', on=['touchpoint_hier_l2_cd','cal_sun_wk_ending_dt'])


# In[ ]:

alb_cos_loy.head()


# In[ ]:

alb_cos_loy.info()


# In[ ]:

#Changing columns names
alb_cos_loy.rename(columns={'touchpoint_hier_l2_desc':'chain_nm','touchpoint_hier_l2_cd':'chain_nbr','dollars_x':'dollars','trips_x':'trips','dollars_y': 'loyalty_dollars', 'trips_y': 'loyalty_trips'}, inplace=True)


# In[ ]:

#Remove the exponential notation
pd.set_option('display.float_format', lambda x: '%.0f' % x)


# In[ ]:

'''#Formatting the dollar column
def commadollar(x):
        return "${:,.2f}".format(x)
alb_cos_loy['dollars']=alb_cos_loy['dollars'].apply(commadollar)
alb_cos_loy['loyalty_dollars']=alb_cos_loy['loyalty_dollars'].apply(commadollar)'''


# In[ ]:

#Convert object to int
alb_cos_loy['trips']=alb_cos_loy['trips'].astype(str).astype(int)
alb_cos_loy['loyalty_trips']=alb_cos_loy['loyalty_trips'].astype(str).astype(int)
alb_cos_loy['loyalty_ids']=alb_cos_loy['loyalty_ids'].astype(str).astype(int)


# In[ ]:

'''#Formatting the trip column
def comma(x):
        return "{:,}".format(x)
alb_cos_loy['trips']=alb_cos_loy['trips'].apply(comma)
alb_cos_loy['loyalty_trips']=alb_cos_loy['loyalty_trips'].apply(comma)
alb_cos_loy['loyalty_ids']=alb_cos_loy['loyalty_ids'].apply(comma)'''


# In[ ]:

#sort by below columns
alb_cos_loy=alb_cos_loy.sort_values(by=['chain_nm','chain_nbr','cal_sun_wk_ending_dt'])


# In[ ]:

#Saving the data frame to a new excel directly
from pandas import ExcelWriter
writer = ExcelWriter('alb_cos_loy ' + max(total['cal_sun_wk_ending_dt'])+ '.xlsx')
alb_cos_loy.to_excel(writer,'Sheet1',index=False)
writer.save()


# In[ ]:

newreport='alb_cos_loy ' + max(total['cal_sun_wk_ending_dt'])+ '.xlsx'


# In[ ]:

newreport


# In[ ]:

#get the path of original file
reportpath=os.path.abspath('alb_cos_loy ' + max(total['cal_sun_wk_ending_dt'])+ '.xlsx')


# In[ ]:

#load file data
savedreport = openpyxl.load_workbook(reportpath)
savedsheet = savedreport.get_sheet_by_name('Sheet1')


# In[ ]:

#just to see a value in the cell
savedsheet.cell(2,4).value


# In[ ]:

#Number of rows in the report.
i=1
totalrows=0
for item in range(0,10000):
    if savedsheet.cell(i,1).value == None:
        totalrows=i-1
        break
    else:
        i=i+1
totalrows


# In[ ]:

#change the format for $ column
def pasteRange(Col,startRow,endRow,sheetReceiving):
    for i in range(startRow,endRow+1,1):           
            sheetReceiving.cell(row = i, column = Col).number_format = '"$"#,##0.00_);("$"#,##0.00)'


# In[ ]:

pasteRange(4, 2,totalrows,savedsheet)
pasteRange(6, 2,totalrows,savedsheet)


# In[ ]:

#change the format for trip columns
def pasteRange(Col,startRow,endRow,sheetReceiving):
    for i in range(startRow,endRow+1,1):           
            sheetReceiving.cell(row = i, column = Col).number_format = '#,##0'


# In[ ]:

pasteRange(5, 2,totalrows,savedsheet)
pasteRange(7, 2,totalrows,savedsheet)
pasteRange(8, 2,totalrows,savedsheet)


# In[ ]:

#adjusting the width of cells
for col in savedsheet.columns:
    if len(str(col[1].value)) > len(str(col[0].value)):
        max_length=len(str(col[1].value))
        adjusted_width = (max_length + 2) * 1.2
        column_name=col[0].column
        savedsheet.column_dimensions[column_name].width = adjusted_width       
    else:
        max_length=len(str(col[0].value))
        adjusted_width = (max_length + 2) * 1.2
        column_name=col[0].column
        savedsheet.column_dimensions[column_name].width = adjusted_width 


# In[ ]:

savedreport.save('alb_cos_loy ' + max(total['cal_sun_wk_ending_dt'])+ '.xlsx')


# In[ ]:

import smtplib as sm
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders


# In[ ]:

email_user='suhas.nadiga@catalina.com'
email_to='Dwayne.Franko@catalina.com'
email_cc=['Jingmin.Rogers@catalina.com','suhas.nadiga@catalina.com','Rong.Li@catalina.com','Yimei.Rogier@catalina.com','Eric.Bird@catalina.com']
subject='Albertson COS Loyalty Report '+max(total['cal_sun_wk_ending_dt'])


# In[ ]:

rcpt=[email_to]+email_cc
msg=MIMEMultipart()
msg['From']=email_user
msg['To']=email_to
msg['Cc']=",".join(email_cc)
msg['Subject']=subject


# In[ ]:

body="Attached is the Albertson COS Loyalty Report"
msg.attach(MIMEText(body,'plain'))

filename=newreport
attachment=open(filename,'rb')

part=MIMEBase('application','octet-stream')
part.set_payload((attachment).read())
encoders.encode_base64(part)
part.add_header('Content-Disposition',"attachment; filename= "+filename)


# In[ ]:

msg.attach(part)
text=msg.as_string()
server=sm.SMTP('smtp.office365.com',587)
server.starttls()


# In[ ]:

server.login(userid,password) #----Please provide your Catalina ID (Ex: snadiga@catmktg.com) and password


# In[ ]:

server.sendmail(email_user,rcpt,text)
server.quit()


# In[ ]:



