
# coding: utf-8

# In[4]:

#Make sure jaydebeapi is version 0.2.0
import jaydebeapi as jd
import pandas as pd
#import required packages
import openpyxl
import xlwt
import os
import datetime
from connect import UID,PWD
from connections import conn_string, jdbc_nzo, driver, jar_path
from email_credentials import userid,password

#This line establishes the connection to the server
conn = jd.connect(driver, jdbc_nzo, jar_path)
cursor = conn.cursor()         #open a cursor     


# In[ ]:

#Define your query here using standard SQL
query_3 ="""select distinct a.Campaign
		, Reward_Type
		, Offer_Description
		, a.Offer_nbr
		, Start_dt
		, Stop_dt
		, Reward_min
		, Reward_Value
		, Manufacturer
		, a.Division
		, case when a.Division = 'Denver' and sum_chn = 453 then 'Albertsons & Safeway'
				when a.Division = 'Denver' and sum_chn = 433 then 'Albertsons'
				when a.Division = 'Denver' and sum_chn = 20 then 'Safeway'
				when a.Division = 'Acme' and sum_chn = 100 then 'Albertsons'
				when a.Division = 'Eastern' and sum_chn = 17 then 'Safeway'
				when a.Division = 'Houston' and sum_chn = 870 then 'Albertsons & Safeway'
				when a.Division = 'Houston' and sum_chn = 434 then 'Albertsons'
				when a.Division = 'Houston' and sum_chn = 436 then 'Safeway'
				when a.Division = 'Southwest' and sum_chn = 363 then 'Albertsons & Safeway'
				when a.Division = 'Southwest' and sum_chn = 275 then 'Albertsons'
				when a.Division = 'Southwest' and sum_chn = 88 then 'Safeway'
				when a.Division = 'Seattle' and sum_chn = 302 then 'Albertsons & Safeway'
				when a.Division = 'Seattle' and sum_chn = 284 then 'Albertsons'
				when a.Division = 'Seattle' and sum_chn = 18 then 'Safeway'
				when a.Division = 'Portland' and sum_chn = 530 then 'Albertsons & Safeway'
				when a.Division = 'Portland' and sum_chn = 435 then 'Albertsons'
				when a.Division = 'Portland' and sum_chn = 95 then 'Safeway'
				when a.Division = 'Southern' and sum_chn = 291 then 'Albertsons & Safeway'
				when a.Division = 'Southern' and sum_chn = 277 then 'Albertsons'
				when a.Division = 'Southern' and sum_chn = 14 then 'Safeway'
				when a.Division = 'SoCal' and sum_chn = 12 then 'Albertsons & Safeway'
				when a.Division = 'SoCal' and sum_chn = 8 then 'Albertsons'
				when a.Division = 'SoCal' and sum_chn = 4 then 'Safeway'
				when a.Division = 'Intermountain' and sum_chn = 715 then 'Albertsons & Safeway'
				when a.Division = 'Intermountain' and sum_chn = 278 then 'Albertsons'
				when a.Division = 'Intermountain' and sum_chn = 437 then 'Safeway'
				when a.Division = 'Jewel' and sum_chn = 104 then 'Albertsons'
				when a.Division = 'NorCal' and sum_chn = 15 then 'Safeway'
				when a.Division = 'United' and sum_chn = 441 then 'Albertsons'
				when a.Division = 'Shaws' and sum_chn = 240 then 'Albertsons'
				else 'NAN' end as Banner
		, Salesperson
		, pgm_administrator_nm
from 
(select pgm_nm as Campaign
		, promo_desc_txt as Offer_Description
		, promo_src_id_txt as Offer_nbr
		, case when promo_billing_prod_nm is not null then promo_billing_prod_nm 
			when promo_billing_prod_nm is null and coalesce(promo_cat_inctv_typ_cd,promo_ncat_inctv_typ_cd) = 'ANNC' and promo_cpblty_typ_cd = 'CCM' then 'CCM Announcement' 
			when promo_billing_prod_nm is null and coalesce(promo_cat_inctv_typ_cd,promo_ncat_inctv_typ_cd) = 'RWD' and promo_cpblty_typ_cd = 'CCM' then 'CCM Reward'
			when promo_billing_prod_nm is null and promo_cpblty_typ_cd = 'LOY-RWDS' then 'Loyalty Rewards' 
			else 'Advertisement' end as Reward_Type
		, client_nm as Manufacturer
		--, coalesce(barcd_promo_issuer_nm,client_barcd_issuer_nm,client_nm) as Manufacturer
		, coalesce(cnsmr_curr_beh_purch_rqmt_qty,cnsmr_trg_class_purch_rqmt_qty) as Reward_min
		, coalesce(promo_discnt_val_amt,crtv_discnt_val_amt) as Reward_Value
		, case when csdb_chn_nbr in (278,437) then 'Intermountain'
				when csdb_chn_nbr in (100) then 'Acme'
				when csdb_chn_nbr in (104) then 'Jewel'
				when csdb_chn_nbr in (240) then 'Shaws'
				when csdb_chn_nbr in (17) then 'Eastern'
				when csdb_chn_nbr in (15) then 'NorCal'
				when csdb_chn_nbr in (88,275) then 'Southwest'
				when csdb_chn_nbr in (277,14) then 'Southern'
				when csdb_chn_nbr in (284,18) then 'Seattle'
				when csdb_chn_nbr in (433,20) then 'Denver'
				when csdb_chn_nbr in (434,436) then 'Houston'
				when csdb_chn_nbr in (435,95) then 'Portland'
				when csdb_chn_nbr in (441) then 'United'
				when csdb_chn_nbr in (8,4) then 'SoCal'
				else 'No Division' end as Division
		, fin_cmit_svc_ae_first_nm || ' ' || fin_cmit_svc_ae_last_nm as salesperson
		, pgm_administrator_nm
		, min(promo_dist_start_dt) as Start_dt
		, max(promo_dist_stop_dt) as Stop_dt
from ord_promo_varnt_fact_v o
join promotion_variant_v p using (promo_varnt_key)
join touchpoint_v tp on ord_touchpoint_key=tp.touchpoint_key
where (promo_billing_prod_id in ('USA-CATS-500','USA-CATS-523','USA-CATS-525','USA-CATS-506','USA-CATS-550','USA-CATS-549')
or promo_cpblty_typ_cd in ('CCM','CATTGTAD','LOY-RWDS')
/*or promo_nm like '%CCM%'*/)
and promo_cpblty_typ_cd <> 'BLIND'
and csdb_chn_nbr in (8,100,104,240,275,278,284,433,434,435,4,14,15,17,18,20,88,95,217,436,437,441)
and promo_deliv_purpose_cd <> 'SIMULATION'
and promo_dist_stop_dt >= now()
group by 1,2,3,4,5,6,7,8,9,10) a
join 
(select division
		, pgm_nm as Campaign
		, promo_src_id_txt as Offer_nbr
		, sum(csdb_chn_nbr) as sum_chn
from 
(select distinct pgm_nm
		, csdb_chn_nbr
		, promo_src_id_txt
		, case when csdb_chn_nbr in (278,437) then 'Intermountain'
				when csdb_chn_nbr in (100) then 'Acme'
				when csdb_chn_nbr in (104) then 'Jewel'
				when csdb_chn_nbr in (240) then 'Shaws'
				when csdb_chn_nbr in (17) then 'Eastern'
				when csdb_chn_nbr in (15) then 'NorCal'
				when csdb_chn_nbr in (88,275) then 'Southwest'
				when csdb_chn_nbr in (277,14) then 'Southern'
				when csdb_chn_nbr in (284,18) then 'Seattle'
				when csdb_chn_nbr in (433,20) then 'Denver'
				when csdb_chn_nbr in (434,436) then 'Houston'
				when csdb_chn_nbr in (435,95) then 'Portland'
				when csdb_chn_nbr in (441) then 'United'
				when csdb_chn_nbr in (8,4) then 'SoCal'
				else 'No Division' end as Division

from ord_promo_varnt_fact_v o
join promotion_variant_v p using (promo_varnt_key)
join touchpoint_v tp on ord_touchpoint_key=tp.touchpoint_key
where (promo_billing_prod_id in ('USA-CATS-500','USA-CATS-523','USA-CATS-525','USA-CATS-506','USA-CATS-550','USA-CATS-549')
or promo_cpblty_typ_cd in ('CCM','CATTGTAD','LOY-RWDS')
/*or promo_nm like '%CCM%'*/)
and promo_cpblty_typ_cd <> 'BLIND'
and csdb_chn_nbr in (8,100,104,240,275,278,284,433,434,435,4,14,15,17,18,20,88,95,217,436,437,441)
and promo_deliv_purpose_cd <> 'SIMULATION'
and promo_dist_stop_dt >= now() ) z
group by 1,2,3 ) b on a.Campaign=b.Campaign and a.division=b.division and a.Offer_nbr=b.Offer_nbr
order by 1,4;"""


ccm_query = pd.read_sql_query(query_3, conn)

#Close the connection
cursor.close() #close the cursor
conn.close()


# In[ ]:

ccm_query.head()


# In[ ]:

#Saving the data frame to a new excel directly
from pandas import ExcelWriter
import xlsxwriter
from datetime import datetime, timedelta

days_to_subtract = 2
now = datetime.now().date() - timedelta(days=days_to_subtract)

writer = pd.ExcelWriter('CCM Report ' + now.strftime('%m-%d-%Y') + '.xlsx', engine = 'xlsxwriter')
ccm_query.to_excel(writer,'Overall',index=False)


# In[ ]:

category=ccm_query['division'].unique()
d={}
for i in category:
    d[str(i)]=ccm_query[ccm_query['division']==i]
for key, value in d.iteritems():
    value.to_excel(writer,key,index=False)
writer.save()


# In[ ]:

#load file data
ccmpath=os.path.abspath('CCM Report ' + now.strftime('%m-%d-%Y') + '.xlsx')
ccmreport = openpyxl.load_workbook(ccmpath)


# In[ ]:

totalrows = len(ccm_query)
totalrows


# In[ ]:

#change the format for $ column
def pasteMoney(Col,startRow,endRow,sheetReceiving):
    for i in range(startRow,endRow+1,1):           
            sheetReceiving.cell(row = i, column = Col).number_format = '"$"#,##0.00_);("$"#,##0.00)'
            
#change the format for $ column
def pasteNumber(Col,startRow,endRow,sheetReceiving):
    for i in range(startRow,endRow+1,1):           
            sheetReceiving.cell(row = i, column = Col).number_format = '#,##0'


# In[ ]:

d.update({'Overall':ccm_query})
for key, value in d.iteritems():
    ccmsheet = ccmreport.get_sheet_by_name(key)
    pasteMoney(9, 1,totalrows,ccmsheet)
    pasteNumber(8, 1,totalrows,ccmsheet)
    #adjusting width of cells
    for col in ccmsheet.columns:
        if len(str(col[1].value)) > len(str(col[0].value)):
            max_length=len(str(col[1].value))
            adjusted_width = (max_length + 2) * 1.2
            column_name=col[0].column
            ccmsheet.column_dimensions[column_name].width = adjusted_width       
        else:
            max_length=len(str(col[0].value))
            adjusted_width = (max_length + 2) * 1.2
            column_name=col[0].column
            ccmsheet.column_dimensions[column_name].width = adjusted_width 
    


# In[ ]:

ccmreport.save('CCM Report ' + now.strftime('%m-%d-%Y') + '.xlsx')


# In[ ]:

reportname='CCM Report ' + now.strftime('%m-%d-%Y') + '.xlsx'
reportname


# In[ ]:

import smtplib as sm
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders


# In[ ]:

email_user='suhas.nadiga@catalina.com'
email_to='Nancy.Coomler@catalina.com'
email_cc=['suhas.nadiga@catalina.com','Eric.Bird@catalina.com','Helen.Rogers@catalina.com','Julie.Diano@catalina.com','Dwayne.Franko@catalina.com','Yimei.Rogier@catalina.com','Rong.Li@catalina.com']
subject='CCM Report '+now.strftime('%m-%d-%Y')


# In[ ]:

rcpt=[email_to]+email_cc
msg=MIMEMultipart()
msg['From']=email_user
msg['To']=email_to
msg['Cc']=",".join(email_cc)
msg['Subject']=subject


# In[ ]:

body="Attached is the CCM Report"
msg.attach(MIMEText(body,'plain'))

filename=reportname
attachment=open(filename,'rb')

part=MIMEBase('application','octet-stream')
part.set_payload((attachment).read())
encoders.encode_base64(part)
part.add_header('Content-Disposition',"attachment; filename= "+filename)


# In[ ]:

msg.attach(part)
text=msg.as_string()
server=sm.SMTP('smtp.office365.com',587)
server.starttls()


# In[ ]:

server.login(userid,password) #----Please provide your Catalina ID (Ex: snadiga@catmktg.com) and password


# In[ ]:

server.sendmail(email_user,rcpt,text)
server.quit()


# In[ ]:



